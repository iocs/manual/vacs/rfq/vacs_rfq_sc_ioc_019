#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_halrcrga
#
require vac_ctrl_halrcrga


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_halrcrga_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: RFQ-010:Vac-VGR-40000
# Module: vac_ctrl_halrcrga
#
iocshLoad("${vac_ctrl_halrcrga_DIR}/vac_ctrl_halrcrga_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VGR-40000, CONTROLLERNAME = RFQ-010:Vac-VERA-40001, IPADDR = moxa-vac-rfq-1.tn.esss.lu.se, PORT = 4004")

#- Temporary access security
asSetFilename("$(E3_CMD_TOP)/vacuum.acf")
asSetSubstitutions("HOSTNAME=$(HOSTNAME)")
