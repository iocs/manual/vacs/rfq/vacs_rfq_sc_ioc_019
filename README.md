# IOC for RFQ vacuum residual gas analyzer

## Used modules

*   [vac_ctrl_halrcrga](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_halrcrga)


## Controlled devices

*   RFQ-010:Vac-VERA-40001
    *   RFQ-010:Vac-VGR-40000
